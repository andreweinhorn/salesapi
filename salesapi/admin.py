from django.contrib import admin
from salesapi.models import Transaction

# Register your models here.

class TransactionAdmin(admin.ModelAdmin):
    list_display = ["id", "date", "transaction_type", "country", "input_currency", "input_net", "input_vat", "eur_net", "eur_vat"]

admin.site.register(Transaction, TransactionAdmin)