from django.core.exceptions import ValidationError
import re
from datetime import datetime
import pycountry

# Create mapping of countries to country codes
mapping =  {country.name.upper(): country.alpha_2 for country in pycountry.countries}
mapping['GREAT BRITAIN'] = 'GB'

def clean_transactions(data):
    """
    Ensures that the transaction is correctly  identified as a purchase or sale based on 
    a simplified regular expression.  Ensures that the correct country code is captured 
    regardless of whether ISO code is given or country name.  Correctly formats the date.
    Identifies the list of exchange rates required.
    """
    # Create arrays for clean transactions and a set of currencies
    cleaned_transactions = []           
    currencies_used = set()

    # For each line, modify the data to match expect format and validate
    line_number = 1
    for row  in data:
        raw_date = row['Date']
        raw_transaction_type = row['Purchase/Sale'].lower()
        raw_country = row['Country'].upper()
        raw_currency = row['Currency']
        raw_net = row['Net']
        raw_vat = row['VAT']

        # Validate and set the purchase field
        if bool(re.search('p.*?rch.*?', raw_transaction_type)):
            transaction_type = 'purchase'
        elif bool(re.search('s.*?l.*?', raw_transaction_type)):
            transaction_type = 'sale'
        else:
            raise ValidationError(
                f'Invalid transaction type: {raw_transaction_type}s in line {str(line_number)}.',
                code='invalid',
            )
        
        # Validate and set the country  field
        if raw_country in mapping.keys():
            country = mapping[raw_country]
        elif raw_country in mapping.values():
            country = raw_country
        else:
            raise ValidationError(
                f'Invalid country: {raw_country} in line {str(line_number)}.',
                code='invalid',
            )

        # Replace / with - in the date string
        date = raw_date.replace('/', '-')

        # Check the date format is correct
        format = "%Y-%m-%d"
        try:
            datetime.strptime(date, format)
        except ValueError:
            # TODO: Implement logging module and log error
            continue


        # Add currency to pool of currency conversions required
        currencies_used.add(raw_currency)

        # Generate transaction object with cleaned data
        transaction = {}
        transaction['date'] = date
        transaction['transaction_type'] = transaction_type
        transaction['country'] = country
        transaction['input_currency'] = raw_currency
        transaction['input_net'] = raw_net
        transaction['input_vat'] = raw_vat

        cleaned_transactions.append(transaction)
        line_number+=1

    return cleaned_transactions, currencies_used 