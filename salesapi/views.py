from django.shortcuts import render
from rest_framework import serializers, viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from salesapi.models import Transaction, COUNTRIES
import csv
import codecs
import requests
import xml.etree.ElementTree as ET
from salesapi.utils import clean_transactions
from datetime import datetime
from django.http import JsonResponse

country_codes = [country[0] for country in COUNTRIES]

class TransactionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Transaction
        fields = ['id', 'date', 'transaction_type', 'country', 'input_currency', 'input_net', 'input_vat', 'eur_net', 'eur_vat', 'creation_date']

class TransactionViewSet(viewsets.ModelViewSet):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer

    @action(detail=False, methods=['POST'])
    def processFile(self, request):
        """
        Takes as input a request object which includes a csv file called
        'file' which contains a list of transactions and ingests these
        transactions into teh database.

        For each line in the csv, modifies the data to match the format expected
        in the database.  Then runs the serializer against cleaned data and 
        creates the entries using bulk_create to reduce load on database.
        """

        # Grab the csv from the request object
        file = request.FILES.get('file')
        reader = csv.DictReader(codecs.iterdecode(file, "utf-8"), delimiter=",")
        data = list(reader)

        # Run data through a util function which cleans the data
        cleaned_transactions, currencies_used = clean_transactions(data)
        
        # Create and validate serialized cleaned_transactions
        serializer = self.serializer_class(data=cleaned_transactions, many=True)
        serializer.is_valid(raise_exception=False)

        # Ping the European Bank api for the most recent exchange rates needed
        currencies_string = '+'.join(list(currencies_used))
        query_string = 'https://sdw-wsrest.ecb.europa.eu/service/data/EXR/M.' + currencies_string + '.EUR.SP00.A?lastNObservations=1'
        r = requests.get(query_string)
        root = ET.fromstring(r.text)

        # Extract the currencies and exchange rates from the xml
        currencies = root.findall('''{*}DataSet/{*}Series/{*}SeriesKey/{*}Value/[@id='CURRENCY']''')
        exchanges = root.findall('''{*}DataSet/{*}Series/{*}Obs/{*}ObsValue''')

        # Create a dictionary for the exchange rates
        currency_list = [currency.attrib['value'] for currency in currencies]
        exchange_list = [float(exchange.attrib['value']) for exchange in exchanges]
        exchange_dict = dict(zip(currency_list, exchange_list))
        exchange_dict['EUR'] = 1

        # Create Transaction objects to be saved to database
        transaction_list = []
        for item in serializer.data:
            transaction = Transaction(
                date=item['date'],
                transaction_type=item['transaction_type'],
                country=item['country'],
                input_currency=item['input_currency'],
                input_net=float(item['input_net']),
                input_vat=float(item['input_vat']),
            )
            if item['input_currency'] in exchange_dict.keys():
                exchange_rate = exchange_dict[item['input_currency']]
                transaction.eur_net = transaction.input_net/exchange_rate
                transaction.eur_vat = transaction.input_vat/exchange_rate
            transaction_list.append(transaction)

        # Save transaction list to database
        Transaction.objects.bulk_create(transaction_list)
        
        return Response("Successfully uploaded the file.")


    @action(detail=False, methods=['GET'])
    def retrieveRows(self, request):
        country = request.query_params.get('country')
        date = request.query_params.get('date')

        # Notify user if required parameters have not been included
        if not country:
            return Response(data="Must provide a country parameter in request.", status=status.HTTP_400_BAD_REQUEST)
        if not date:
            return Response(data="Must provide a date parameter in request.", status=status.HTTP_400_BAD_REQUEST)

        country = country.upper()
        date = date.replace('/', '-')

        # Check country is valid
        if not country in country_codes:
            return Response("Invalid country code provided.")

        # Check date is valid
        format = "%Y-%m-%d"
        try:
            datetime.strptime(date, format)
        except ValueError:
            return Response("Invalid date provided.")

        # Filter transactions and serialize
        transactions = Transaction.objects.filter(date=date, country=country)
        serializer = TransactionSerializer(transactions, many=True)

        return JsonResponse({'data': serializer.data})
