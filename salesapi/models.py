from django.db import models
from salesapi.currencies import CURRENCIES
import pycountry

TRANSACTION_TYPES = (
    ('purchase', 'Purchase'),
    ('sale', 'Sale')
)

COUNTRIES = [(country.alpha_2, country.name) for country in list(pycountry.countries)]


class Transaction(models.Model):
    # Date of transaction
    date = models.DateField()

    # Transaction type is sale or purchase
    transaction_type = models.CharField(
        choices=TRANSACTION_TYPES,
        max_length = 20,
    )

    # Country is represented by the two character ISO code
    country = models.CharField(
        choices = COUNTRIES,
        max_length=2
    )

    # Currency, amount and vat amount of transaction in original currency
    input_currency = models.CharField(
        choices = CURRENCIES,
        max_length=3
    )
    input_net = models.DecimalField(max_digits=12, decimal_places=2)
    input_vat = models.DecimalField(max_digits=12, decimal_places=2)

    # Converted amount and vat amount in Euro
    eur_net = models.DecimalField(blank=True, null=True, max_digits=12, decimal_places=2)
    eur_vat = models.DecimalField(blank=True, null=True, max_digits=12, decimal_places=2)
    
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name="Creation Date")



