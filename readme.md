## Requirements

This project uses Docker to maintain its environment.  To run the project, you will need a copy of [Docker](https://docs.docker.com/get-docker/) installed on your machine .  This documentation assumes you are working on a Mac or in a similar Unix based environment, but the equivalent steps on a Windows machine should work fine.

## Getting Started

In an appropriate folder on your computer:

```
mkdir salesapi    
cd salesapi    
git clone https://gitlab.com/andreweinhorn/salesapi.git .    
```

NB. Don't forget the '.' on the end of the `git clone` line above

```
docker-compose build
docker-compose up
```

In a separate tab in the same folder, run `docker ps` to obtain the container ID.  It should look something like `d40eca23ab5f`.  Then run

```
docker exec -it <container-id> bash
python manage.py migrate
```

You should now be able to view the transactions api at `http://localhost:8000/transactions/`

<br/>

## POST Requests

Using Insomnia or Postman or equivalent, post the medium.csv file to the following URL

POST:  http://localhost:8000/transactions/processFile/ with file attachment

NB. Don't forget the / on the end of the above URL

<br/>

## GET Requests

GET:  http://localhost:8000/transactions/retrieveRows?country=CZ&date=2020/01/01

The above GET request should return six transactions corresponding the transactions made in the Czech Republic on 1 January 2020.

<br/>

## Notes

Behaviours
1.  If a date is incorrectly formatted, the ingestion will skip that entry.  Ideally we would want to introduce logging to capture instances of when this happens and provide feedback to the sender on the affected rows.
2.  We use the regex /p.*?rch*.?/ to identify transactions that are purchases (case insensitive).  
3.  We use the regex /s.*?l*.?/ to identify transactions that are sales (case insensitive).  
4.  If a currency is not available from the European Bank API but all other information is correct, we capture that transaction but do not store its Euro converted amounts.  
5.  We store the input net and vat amounts as well as the input currency in addition to the converted Euro amounts.  
6.  We make a single query to the European Bank exchange rate API but pooling together the exchange rates needed into a single request.
7.  We use django's bulk_create function to create the database entries to reduce the load on the database when ingesting a large number of transactions.
8.  We use two external libraries and their dependencies:  pycountry and requests.  
9.  To the greatest extent possible we clean the data, inferring the sender's intention.  However, if there is an error which the cleaning process does not recognise, none of the data will be ingested and a descriptive error will be returned to the sender to allow them to clean the data with the contextual knowledge they have.

<br/>

## Discussion

The potential bottlenecks here are (a) loading large sets of data into the database and (b) large numbers of queries to the exchange rate API.  

With respect to exchange rates, if one were to query the exchange rate API for every transaction you wanted to load into the database, this would take a very long time and you would likely hit a rate limit on queries to that API.  Instead of one query per transaction, we can identify all of the currencies needed for a single CSV upload (e.g. GBP, USD, ZAR, CZK) and retrieve all of them in a single query to the API.  Instead of using a 100,000 queries for a csv containing 100,000 rows, we can obtain the exchange rate data with one query.

With respect to loading large datasets into the database, if we save each row one at a time, we will hit the database 100,000 times for that many rows.  Instead we use django's bulk_create function to load data using a single query instead of multiple.

Not implemented here but worth considering is the possibility of loading data asynchronously.  For a very large dataset, a user might be frustrated having to wait minutes (or even hours) to receive feedback on their upload.  

The upload times using the current setup were:

Small CSV:    300ms    
Medium CSV:   3s
Large CSV:    30s

Data cleaning and validation presents a trade-off between asking the user to correct every line that does not match the expected input data, and inferring what the user meant when they uploaded the data.  To this end, I used regular expressions to try to understand the user's intent between purchase and sale, and used the pycountry package to deal with variations in how country is captured.  In the case of incorrectly formatted dates, instead of returning an error to the user the api simply skips that row.  Ideally it should skip the row but return a message to the user mentioning which rows have been skipped.

I found the exchange rate API a little difficult to understand and the documentation on it a bit sparse.  I am not sure that I am using the ideal query to obtain the exchange rates, or am extracting the information from the xml in the most efficient/elegant way, but it works.  I also noticed that the API provides a limited number of exchange rates.  For example, it does not provide AED (the Emirati Dirham) so I captured those transactions but left the Euro conversions out.  I could have used a different exchange rate API but for the purposes of the assignment, stuck to the one suggested.

